'use strict';


const http = require('http');


const express = require('express');
const path = require('path');
const bodyParser = require('body-parser')

const HttpServer = class{
    
        constructor(port){
            console.log(' der port ist: ', port);
            this.port = port ? port : 3000; 
            if (process.env.HEROHTTP == 1 && process.env.HEROKEEPONLINE == 1 && process.env.HEROSUBURL){
                setInterval(function() {
                    http.get("http://"+process.env.HEROSUBURL+".herokuapp.com");
                }, 5 * 60 * 1000);
            }
        }
    
        startWithInMemory(tweetStatistics){
    
            const app = express();   
            const server = http.createServer(app);                         
            app.use(bodyParser.json());
            
            app.get('/api/tweetStatistics/:type', (req, res) => {
                
                if (req.params.type == 'tweetsPerHour'){
                    res.json( tweetStatistics.map(item => { return ({time: item.time, timeLimit: item.timeLimit, counter: item.tweetsPerHour})} ));
                }
                else if (req.params.type == 'numberOfUsersTalking'){
                    res.json( tweetStatistics.map(item => { return ({time: item.time, timeLimit: item.timeLimit, counter: item.numberOfUsersTalking})} ));
                }
                else{
                    res.status(404).json( {message: 'service not available'});
                    
                }
            });
                
            
            app.use('/view', express.static(path.resolve(__dirname, "../client/public/")));
            app.get('/view/*', (req,res) => {               
                res.sendFile(path.join(__dirname+'/../client/public/index.html'));
            });
            app.use('/bundles', express.static(path.resolve(__dirname, "../client/bundles/")));
            
            server.listen(this.port, () => {
                console.log('Server listening on port ', this.port);
            });
    
    
        }
    
    }
    
    module.exports = HttpServer;