'use strict';

const http = require('http');
const https = require('https');

const TwitterApi = require('./TwitterApi');
const HttpServer = require('./HttpServer');

const relevantTweets = [
    'IoT', 'InternetOfThings', 'Web4.0', 'Digitalization', 'RaspberryPi'
];
   
const port = process.env.PORT || 3000;
console.log(' port in app.js: ', port);

const twitter = new TwitterApi({ listSize: 20, intervalSec: 60 * 60, timeRangeSec: 60*30,  relevantTweets });
twitter.startFetchTwitterData();


const httpserve = new HttpServer(port);
httpserve.startWithInMemory(twitter.tweetStatistics);










