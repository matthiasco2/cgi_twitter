'use strict';

const http = require('http');

const Twitter = require('twitter');


const TwitterApi = class{
    
        constructor({ listSize, intervalSec, timeRangeSec, relevantTweets}){
            this.listSize = listSize ? listSize : 10 ;
            this.tweetStatistics = [];
            this.relevantTweets = relevantTweets ? relevantTweets : [];
            this.intervalSec = intervalSec ? intervalSec : 60 * 60;
            this.timeRangeSec = timeRangeSec ? timeRangeSec : 60 * 5;
        }
    
        startFetchTwitterData(){


            let client = new Twitter({
                consumer_key: 'Il1pYoNzs9ET6DL82fgmzmU4F',
                consumer_secret: 'Za7wbnDCCG9IPVfm2n4atcp3nq4WTWjTjXBpL2pe1ZIpCTzAso',
                access_token_key: '1085820421583319040-boiDXBTWsDhQy86F8kwKR6l4yQsKcB',
                access_token_secret: 'iftChevDm4Q0G4K3v8qYjVY5QchXsNYUwQm2Vp7dOkuuo'
            });
              

           



            let getTwitterData = () => {

                let timeNow = Date.now();
                let timeRange = 1000 * this.timeRangeSec;   
                let timeLimit = timeNow - timeRange;
                let countTweets = 0;
                let userCounter = {};

                let callTwitterNextBatch = function(itemId, countTweets, timeLimit, timeNow, res, rej){
                    
                    let batchCount = 0;
    
                    client.get('search/tweets', {q: 'IoT', result_type: 'recent', max_id: itemId, count: 90 }, function(error, tweets, response) {
                        if (error){
                            return rej(error);                            
                        }
                        for (let idx in tweets.statuses) {
                            let item = tweets.statuses[idx];
                            console.log(idx, ' : ', item.text.substr(0,20), item.created_at);
                            
                            let itemDate = Date.parse(item.created_at);
                            if (itemDate  > timeLimit){
                                countTweets++;
                                batchCount++;
                                if (!userCounter[item.user.id] ){
                                    userCounter[item.user.id] = 1;
                                }else{
                                    userCounter[item.user.id]++;
                                }
                            
                                if (batchCount == tweets.statuses.length){
                                    return callTwitterNextBatch(item.id, countTweets, timeLimit, timeNow, res, rej); 
                                }
                            }else{
                                console.log(' FINISHED WITH THIS INTERVAL ')
                                let countUsers = 0;
                                for (let i in userCounter){
                                    countUsers += userCounter[i];
                                }
                                return res({countTweets, countUsers, timeNow, timeLimit});
                            }
                        };
                    });
    
                }

                return new Promise((resolve, reject) => {

                    client.get('search/tweets', {q: 'IoT', result_type: 'recent', count: 4 }, function(error, tweets, response) {
                        if (error) {
                            return reject(error);
                        }
                        let startId = Array.isArray(tweets.statuses) &&  tweets.statuses[0] ? tweets.statuses[0].id : null;
                        console.log(startId);
                        
                        if (startId) {
                            countTweets++;
                            
                            userCounter[tweets.statuses[0].user.id] = 1;
                            callTwitterNextBatch(startId, countTweets, timeLimit, timeNow, resolve, reject); 
                        }else{
                            reject('failed to obtain starting tweet');
                        }
                    });

                });
            }

 
            let setTweetStatistics = (data) => {

                let size = this.tweetStatistics.unshift({
                    time: data.timeNow, 
                    timeLimit: data.timeLimit,
                    tweetsPerHour: data.countTweets,
                    numberOfUsersTalking: data.countUsers,
                });
                if (size > this.listSize){
                    this.tweetStatistics.pop();
                } 
            }


            getTwitterData().then(setTweetStatistics).catch(console.log);
            
            setInterval(() => {

                getTwitterData().then(setTweetStatistics).catch(console.log);    
                
            }, 1000 * this.intervalSec);

        }


    } 
    
    


    module.exports = TwitterApi;