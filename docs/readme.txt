INSTALL:

    git clone https://matthiasco2@bitbucket.org/matthiasco2/cgi_twitter.git
    cd cgi_twitter
    npm install
    *** START the fake twitter server : 
    node fakeTwitter/app.js &
    *** BUILD the client JavaScript Packages with webpack and co.:
    npm run build
    *** START the node.js server:
    npm run server

USAGE:

    *** in the Browser navigate to:
    http://localhost:3000/view/
    *** click on either "Anzahl" oder "Tweets" to get the tweet or user numbers 

    *** the api can be reached with, e.g., curl:
    curl http://localhost:3000/api/tweetStatistics/numberOfUsersTalking
    curl http://localhost:3000/api/tweetStatistics/tweetsPerHour

    *** the mock twitter server is here (provisionally):
    curl http://localhost:3001/fakeTwitter


log : 5eeec80101a76c6c58b82280f18d44d9d55c08cc