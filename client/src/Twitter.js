'use strict';

import React from "react";

// this is a tiny replacement for react router, basically only based on history, and using new react hooks
import useBrowserHistory from './useBrowserHistory';
import Link from "./Link";

import {useState} from 'react';
import DataPresentation from './DataRepresentation';


export default function Twitter() {
    
    const [clicked, setClicked] = useState(false);    
    const {histLocation} = useBrowserHistory();


    return (
        <div>
        <ul>
            <li>  <Link toggleClick={()=>setClicked(!clicked)} href="/tweets"> Tweets pro Zeiteinheit </Link> </li>
            <li>  <Link toggleClick={()=>setClicked(!clicked)} href="/users"> Anzahl der User </Link> </li>
        </ul>
        <hr/>

        
        { histLocation.pathname == '/tweets' && <DataPresentation clicked={clicked} description="Tweets" title="Tweets pro Zeiteinheit zum Thema IoT " type="tweetsPerHour"/> }
        { histLocation.pathname == '/users' && <DataPresentation clicked={clicked} description="Nutzer" title="Anzahl der Nutzer zu einem Tweet" type="numberOfUsersTalking"/> }

        
        </div>
    );
}