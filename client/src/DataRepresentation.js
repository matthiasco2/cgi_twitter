'use strict';
import React from "react";


const readData = function(url, callback){
    fetch(url).then(response => response.json())
            .then(respJson => callback(null, respJson))
            .catch(err => callback(err,null));
}


export default class DataPresentation extends React.Component{
    
    constructor(props){
        super(props);

        this.state = {
            fetchedData: null,
            baseUrl: `${window.location.protocol}//${window.location.hostname}:${window.location.port}/api/tweetStatistics/`,
        }
    }



    componentDidMount(){
        readData(this.state.baseUrl+this.props.type, (err, resp)=>{
        if (err){
            this.setState({fetchedData: null});
            return console.log(err);
        }
        this.setState({fetchedData: resp  });
        });
    }


    componentWillReceiveProps(nextProps){
        //console.log(' in willReceiveProps with ', nextProps, this.props);
        if (nextProps.clicked != this.props.clicked){
            readData(this.state.baseUrl+this.props.type, (err, resp)=>{
                if (err){
                    this.setState({fetchedData: null});
                    return console.log(err);
                }
                this.setState({fetchedData: resp  });
                });
        }
    }

    render(){

        
        return (
        <div className={this.props.type}>
            <h2> {this.props.title} </h2>
            <ul>
            {this.state.fetchedData && 
                this.state.fetchedData.map((item, idx) => {  
                    let now = new Date(item.time).toLocaleTimeString("de-DE");
                    let prev = new Date(item.timeLimit).toLocaleTimeString("de-DE");
                    return <li key={idx}> {this.props.description}: {item.counter} zwischen {prev} und {now}.   </li>
            })}
            </ul>
        </div>
        )
    }
}