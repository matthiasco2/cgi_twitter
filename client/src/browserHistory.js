import createBrowserHistory from 'history/createBrowserHistory';
const browserHistory = createBrowserHistory({forceRefresh: false, basename: '/view'});
export default browserHistory;
