'use strict';

import { useState, useEffect } from 'react';
import browserHistory from "./browserHistory";


export default function useBrowserHistory(){

    const [histLocation, setLocation] = useState(browserHistory.location);
    useEffect(() => {
        return browserHistory.listen(currentLocation => {
            setLocation(currentLocation);
        });
    }, []);  // [] nur beim, ersten mal geladen ( componentDidMount ), nicht wieder, wenn eine prop geaendert wird

    return { histLocation }; 

}