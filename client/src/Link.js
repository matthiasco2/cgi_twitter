'use strict';

import React from "react";
import browserHistory from "./browserHistory";

function Link(props){
    
    let handleClick = (event) => {
    
    const isLeftClick = event.button === 0;
    const isModified = event.metaKey || event.altKey || event.ctrlKey || event.shiftKey;
    const hasNoTarget = !props.target;

    if (isLeftClick && !isModified && hasNoTarget) {
        event.preventDefault();
        props.toggleClick && props.toggleClick();
        const pathname = props.href.split('?')[0];
        const search = props.href.slice(pathname.length);
        const location = {pathname, search};

        //const location = {...browserHistory.location, pathname, search};
        
        browserHistory.push(location);
    }
    }
    
    return (
    <a href={props.href} onClick={handleClick}>
        {props.children}
    </a>
    );
    
}
 

export default Link;